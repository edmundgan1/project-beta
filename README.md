# CarCar

Team:

Elijah Sierra - Service
Edmund Gan - Sales

## How to Run this Application:

Once you have cloned the application from the gitlab, in the terminal run the following commands:
    1. docker volume create pgdata
    2. docker-compose build
    3. docker-compose up
Once the following three commands are ran in that order, the pollers should start running and data will be transmitted from the corresponding micro-services. Once models are created they will begin to populate the list for the corresponding micro-service. The application should be running and you should be able to  get there by including the link below in the browser.

    http://localhost:3000/

You should be able to navigate through the navigation bar on the top
___

Required Applications:

Docker

After cloning the repository with the html Link found in the git Lab, These three commands are needed to run the application:
    1. docker volume create beta-data
    2. docker-compose build
    3. docker-compose up

Once those commands are ran, the Seven containers in the Docker application should be running, as well as the main page should be loaded at http://localhost:3000.


## Application Diagram

![Alt text](CarCar%20Flowchart.png)

### Service

With the carCar application, you can easily track an up to date list of all the appointments that need attention, in this list you can find all the relevant information about an appointment. Key features such as the vin number, owner name, date and time of the appointment, the technician on the job as well the reason for the visit. You can also keep track of vip customers, and cancel or complete appointments from the list view. Another service provided is the ability to create appointments easily provided the necessary information listed above. Once a service is complete the history can be found in the service history form, requiring only a vin number to find all the past and current services regarding a vehicle. New technicians can also be easily added to the roster requiring only a name and the employee number you wish to be assigned.


### Sales

In the sale service, its made easy to create new sales as needed and register customers easily. To register a new customer the only information required are their Name, Address, and Phone Number. After a customer is registered you can complete the form with the price listed, as well as whichever car was available in the inventory. We can also take a look at the history of sales using a dropdown menu of the salesman who made the sale.

___

#### Technicians

    Service URLs and CRUD methods:

    GET (Show Technician Details): http://localhost:8080/api/technicians/1/
    GET (List All Technicians): http://localhost:8080/api/technicians/
    POST (Create Technician): http://localhost:8080/api/technicians/
    PUT (Update Technicians): http://localhost:8080/api/technicians/1/
    DELETE (Delete Technician): http://localhost:8080/api/technicians/1/


    POST Example Body :

    {
	"id": 10,
	"name": "Jorge",
	"employee_number": 123
    }


    PUT Example Body :

    {
	"id": 10,
	"name": "Jorge",
	"employee_number": 1234
    }

#### Appointments

    GET (Show Appointment Details): http://localhost:8080/api/appointments/1/
    GET (List All Appointments): http://localhost:8080/api/appointments/
    POST (Create An Appointment): http://localhost:8080/api/appointments/
    PUT (Update An Appointment): http://localhost:8080/api/appointments/3/
    DELETE (Delete An Appointment): http://localhost:8080/api/appointments/4/


    POST Example Body :

    {
	"vin": "123455",
	"owner": "Smith",
	"date": "2022-12-09",
	"time": "11:50",
	"technician": "10",
	"reason": "wobbly wheel",
	"is_vip": false,
	"completed": false
    }


    PUT Sample Body :

    {
	"vin": "123455",
	"owner": "Smith",
	"date": "2022-12-09",
	"time": "11:50",
	"technician": "10",
	"reason": "Bad Filters",
	"is_vip": false,
	"completed": false
    }

___


#### Sales

    ### Sales URLs and CRUD Methods:

    GET (List SalesMan): http://localhost:8090/api/sales/
    POST (Create A New SalesMan): http://localhost:8090/api/sales/

    GET (List Customers): http://localhost:8090/api/customers/
    POST (Create Customers): http://localhost:8090/api/customers/

    GET (List Sales Record): http://localhost:8090/api/records/
    POST (Create Sales Record) : http://localhost:8090/api/records/
    GET (Get Sales Detail) : http://localhost:8090/api/records/1/



    POST Example Bodies:

    POST Salesman Body :

    {
	"name": "Ed",
	"employee_number":"1"
    }


    POST Customer Body:

    {
	"name": "customer 1",
	"address":"customer address 1",
	"number":"customer number 1"

    }

    POST Sales Record Body:

    {
	"price": 40000,
	"sales_id": 1,
	"customer_id": 1,
	"automobile_id": 1
    }


___
