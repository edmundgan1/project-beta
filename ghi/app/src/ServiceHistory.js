import React, { useState, useEffect} from "react";

const ServiceHistory = () => {
    const [appointments, setAppointments] = useState([])
    const [results, setResults] = useState([])
    const [vin, setVin] = useState('')

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
        .then(response => response.json())
        .then(data => {
            setAppointments(data.appointments)
        })
        .catch(e => console.log('error:', e))
    }, [])

    const search = async (event) => {
        const results = appointments.filter((appointment) =>
        appointment.vin.includes(vin))
        setResults(results)

    }

    return (
        <>
        <h1> Search Vins</h1>
        <div className="container">
            <div className="row">
                <div className="input-group">
                    <input value={vin} onChange={(e) => setVin(e.target.value)} className="form-control" type="text" placeholder="Search Vins" />
                    <button onClick={search} className="btn btn-primary">Search</button>
                </div>
            </div>
            {results.length > 0 && (
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Owner</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>completed</th>
                    </tr>
                </thead>
                <tbody>
                    {results.map((result) => {
                        return(
                            <tr key={result.id}>
                                <td>{result.vin}</td>
                                <td>{result.owner}</td>
                                <td>{result.date}</td>
                                <td>{result.time}</td>
                                <td>{result.technician.name}</td>
                                <td>{result.reason}</td>
                                <td>{result.completed ? "yes":'No'}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            )}
            {results.length <= 0 && (
                <div className="alert alert-danger mb-0 p-4 mt-4" id='danger-message'>
                    No results
                </div>
            )}
        </div>
        </>
    )
}

export default ServiceHistory
