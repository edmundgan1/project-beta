import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm,';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import ManufacturerList from './ManufacturerList';
import VehicleModelList from './VehicleModelList';
import InventoryList from './InventoryList';
import SalesRecordList from './SalesRecordList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory.js';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList manufacturers={props.manufacturers}/>} />
          <Route path="models" element={<VehicleModelList models={props.models}/>} />
          <Route path="automobiles" element={<InventoryList autos={props.autos}/>} />
          <Route path="records" element={<SalesRecordList sales={props.sales}/>} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="sales/new" element={<SalesPersonForm />} />
          <Route path="customer/new" element={<CustomerForm />} />
          <Route path="records/new" element={<SalesRecordForm />} />
          <Route path="models/new" element={<VehicleModelForm />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path='technicians' >
            <Route path='add/' element={<TechnicianForm />} />
          </Route>
          <Route path='appointments' >
            <Route path="" element={<AppointmentList />} />
            <Route path='add/' element={<AppointmentForm />} />
            <Route path='history/' element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
