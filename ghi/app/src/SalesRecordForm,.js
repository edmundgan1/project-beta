import React from 'react';

class SalesRecordForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        price: '',
        sales_id: '',
        sales: [],
        customer_id: '',
        customers: [],
        automobile_id: '',
        automobiles: [],
      };
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleSalesChange = this.handleSalesChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.sales;
        delete data.customers;
        delete data.automobiles;

        const recordUrl = 'http://localhost:8090/api/records/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        }
        const response = await fetch(recordUrl, fetchConfig);
        if (response.ok) {
          const cleared = {
            price: '',
            sales_id: '',
            customer_id: '',
            automobile_id: '',
          };
          this.setState(cleared);
          window.location.reload()
        }
      }

    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({price: value})
    }
    handleSalesChange(event) {
        const value = event.target.value;
        this.setState({sales_id: value})
    }
    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({customer_id: value})
    }
    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({automobile_id: value})
    }


    async componentDidMount() {
        const url = 'http://localhost:8090/api/sales/';
        const url2 = 'http://localhost:8090/api/customers/';
        const url3 = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);
        const response2 = await fetch(url2);
        const response3 = await fetch(url3);
        if (response.ok) {
          const data = await response.json();
          const data2 = await response2.json();
          const data3 = await response3.json();
          this.setState(
            {sales: data.sales,
            customers: data2.customers,
            automobiles: data3.autos
        });

        }
      }

    //   async componentDidMount() {
    //     const url = 'http://localhost:8100/api/automobiles/';

    //     const response = await fetch(url);
    //     if (response.ok) {
    //       const data = await response.json();
    //       console.log(data)
    //       this.setState({
    //         autos: data.autos
    //     });

    //     }
    //   }


    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Sales Record</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePriceChange} placeholder="Name" required
                    type="number" name="price" id="price"
                    className="form-control" value={this.state.name}/>
                    <label htmlFor="price">Price</label>
                  </div>
                  <div className="mb-3">
                  <select onChange={this.handleSalesChange} required
                  name="sales" id="sales" className="form-select">
                  <option value="">Choose a Sales Person</option>
                  {this.state.sales.map(sale => {
                    return (
                      <option key={sale.id} value={sale.id}>
                        {sale.name}</option>
                    )
                  })}
                </select>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleCustomerChange} required
                  name="customer" id="customer" className="form-select">
                  <option value="">Choose a Customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>
                        {customer.name}</option>
                    )
                  })}
                </select>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleAutomobileChange} required
                  name="auto" id="auto" className="form-select">
                  <option value="">Choose an Automobile</option>
                  {this.state.automobiles.map(automobile => {
                    return (
                      <option key={automobile.id} value={automobile.id}>
                        {automobile.vin}</option>
                    )
                  })}
                </select>
                </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

  export default SalesRecordForm;
