import React from 'react';

class CustomerForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        name: '',
        address: '',
        number: '',
      };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        }
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
          const cleared = {
            name: '',
            address: '',
            number: '',
          };
          this.setState(cleared);
          window.location.reload()
        }
      }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
    }
    handleNumberChange(event) {
        const value = event.target.value;
        this.setState({number: value})
    }

    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Sales Person</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required
                    type="text" name="name" id="name"
                    className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleAddressChange} placeholder="Address" required
                    type="text" name="address" id="address"
                    className="form-control" value={this.state.employee_number}/>
                    <label htmlFor="address">Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNumberChange} placeholder="Number" required
                    type="text" name="number" id="number"
                    className="form-control" value={this.state.number}/>
                    <label htmlFor="number">Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

  export default CustomerForm;
