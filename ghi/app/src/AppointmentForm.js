import React, { useEffect, useState } from 'react';

const AppointmentForm = () => {
    const [vin, setVin] = useState("")
    const [owner, setOwner] = useState("")
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [technicians, setTechnicians] = useState([])
    const [technician, setTechnician] = useState("")
    const [reason, setReason] = useState("")

    useEffect(() => {
        const getTechnicians = async () => {
            const url = 'http://localhost:8080/api/technicians'
            const response = await fetch(url)

            if (response.ok) {
                const data = await response.json()
                setTechnicians(data.technicians)
            }
        }
        getTechnicians()
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()
        const newAppointment = {
            'vin': vin,
            'owner': owner,
            'date': date,
            'time': time,
            'technician': technician,
            'reason': reason,
        }

        const url = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newAppointment),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(url, fetchConfig)
        .then(response => response.json())
        .then(() => {
            setVin('')
            setOwner('')
            setDate('')
            setTime('')
            setTechnician('')
            setReason('')
        })
        .catch(e => console.log('error:', e))
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleOwnerChange = (event) => {
        const value = event.target.value
        setOwner(value)
    }

    const handleDateChange = (event) => {
        const value = event.target.value
        setDate(value)
    }

    const handleTimeChange = (event) => {
        const value = event.target.value
        setTime(value)
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value
        setTechnician(value)
    }

    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add Appointment</h1>
                    <form onSubmit={handleSubmit} id='create-appointment-form'>
                        <div className='form-floating mb-3'>
                            <input value={vin} onChange={handleVinChange} required type="text" name="vin" id="vin" className='form-control' />
                            <label htmlFor='vin'>Vin</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={owner} onChange={handleOwnerChange} required type="text" name="owner" id="owner" className='form-control' />
                            <label htmlFor='owner'>Owner</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={date} onChange={handleDateChange} required type="date" name="date" id="date" className='form-control' />
                            <label htmlFor='date'>Date</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input value={time} onChange={handleTimeChange} required type="time" name="time" id="time" className='form-control' />
                            <label htmlFor='time'>Time</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <select value={technician} onChange={handleTechnicianChange} required name="technician" id="technician" className='form-select'>
                            <option value=''>Select Technician</option>
                            {technicians.map((technician) => {
                                return (
                                    <option key={technician.id} value={technician.id}>
                                        {technician.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className='form-floating mb-3'>
                            <textarea value={reason} onChange={handleReasonChange} required type='text' name='reason' id='reason' className='form-control' />
                            <label htmlFor='reason'>Reason</label>
                        </div>
                        <div className='col text-center'>
                            <button className='btn btn-primary'>Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AppointmentForm
